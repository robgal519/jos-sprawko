SHELL:=/bin/bash -O extglob
TARGET=main

ALL=${wildcard *.sty *.tex *.bib *.eps *.png *.jpg *.svg }

all: $(TARGET).pdf

clean:
	rm *.aux *.log *.bbl *.blg *.gz *.toc  || true

deepClean: clean
	rm *.pdf

view: $(TARGET).pdf
	xdg-open $<

$(TARGET).pdf: *.tex tex/*.*
	pdflatex -file-line-error -synctex=1 -shell-escape $(TARGET)
	pdflatex $(TARGET)
	pdflatex $(TARGET)

watch: ## recompile on any update of LaTex or image
	while true; do inotifywait $(ALL); sleep 1; make all; done;
